# R is for Ruby - Practicum

This is a coding practicum designed to test the skills you have (hopefully) learned from the R is for Ruby course at the Software Freedom School. The goal is for you to write a program that ingests a JSON file, calculates a series of metrics, and generates an HTML report that can be viewed in any standard web browser.

## Goals

1. [Read `sample.json` into memory](https://ruby-doc.org/core-3.1.0/IO.html#method-c-read) and parse it using [Ruby's built-in JSON parsing library](https://ruby-doc.org/stdlib-3.1.0/libdoc/json/rdoc/JSON.html). This sample data represents a list of fictional patients we want to generate statistics on.
1. [Write a file](https://ruby-doc.org/core-3.1.0/IO.html#method-c-write) titled `report.html` using [Ruby's built-in templating library](https://ruby-doc.org/stdlib-3.1.0/libdoc/erb/rdoc/ERB.html). We will be writing our calculations to this file.
1. Calculate the percentage of patients who are below 21 years old. Write this statistic to the report under the name "Under 21"
1. Calculate the percentage of patients who are 21 years old or above. Write this statistic to the report under the name "21+"
1. Calculate the percentage of patients who identify as female. Write this statistic to the report under the name "Female".
1. Calculate the percentage of patients who identify as male. Write this statistic to the report under the name "Male".
1. Calculate the percentage of patients who identify as neither female nor male. Write this statistic to the report under the name "Non-Binary".
1. Generate a list of non-binary genders that patients identify as. This list needs to not have duplicate entries, and must be sorted alphabetically. Add this list to the report under the name "Non-Binary Identities".
1. Calculate the lowest weight of any patient. Write this to the report as "Weight Minimum".
1. Calculate the highest weight of any patient. Write this to the report as "Weight Maximum".
1. Calculate the average weight of any patient. Write this to the report as "Weight Average".
1. Finally, generate a table with every patient in the sample data set, including their First Name, Last Name, Gender, Age, and Weight.

## Dependencies

It is assumed that you have Docker installed. This is the only dependency of this project, as all other dependencies will be automatically downloaded by Docker. The primary benefit of using Docker is automatic, reliable dependency management in a way that is portable between operating systems.

https://docs.docker.com/get-docker/

Alternatively, if you have Ruby 3.1.0 installed globally, feel free to try running the program directly without Docker. Different versions may work but the R is for Ruby course will assume you are running that version.

## Running

To run the program, run the following Docker command:

```sh
docker run --rm -it -v "$PWD":/app -w /app ruby:3.1.0-alpine3.15 ./main.rb
```

This command works as-is in Bash, ZSH, Dash, PowerShell, and others.

If you are instead using a globally installed copy of Ruby, you should be able to simply run:

```sh
ruby main.rb
```
